function include() {
	var z, i, elmnt, file, xhttp;
	/* Loop through a collection of all HTML elements: */
	z = document.getElementsByTagName("*");
	for (i = 0; i < z.length; i++) {
		elmnt = z[i];
		/*search for elements with a certain atrribute:*/
		file = elmnt.getAttribute("w3-include-html");
		if (file) {
		/* Make an HTTP request using the attribute value as the file name: */
		xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4) {
				if (this.status == 200) {elmnt.innerHTML = this.responseText;}
				if (this.status == 404) {elmnt.innerHTML = "Page not found.";}
				/* Remove the attribute, and call this function once more: */
				elmnt.removeAttribute("w3-include-html");
				include();
			}

		}
		xhttp.open("GET", file, true);
		xhttp.send();
		/* Exit the function: */
		return;
		}
	}
}

include();

function display_playlists() {
	let xhr = new XMLHttpRequest();

	xhr.open('GET', '/article/xmlhttprequest/example/json');

	xhr.responseType = 'json';

	xhr.send();

	xhr.onload = function() {
		let responseObj = xhr.response;
		alert(responseObj.message);
	};
}

/* Set the width of the side navigation to 250px and the left margin of the page content to 250px and add a black background color to body */
function openNav() {
	document.querySelector("nav").style.width = "250px";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
function closeNav() {
	document.querySelector("nav").style.width = "0";
}
